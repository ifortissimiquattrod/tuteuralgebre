#ifndef VARIABLE_H
#define VARIABLE_H
#include "../expression.h"
#include <string>
#include <map>

using namespace std;

class Variable : public Expression
{
    public:
        virtual ~Variable();
        virtual double eval() const;
        static Variable* getVariable(string name);
        static Variable* getAssg(string name, double value);

    private:
        const string _name;
        double _value;
        Variable(string name, double value);
        Variable& operator=(const Variable&);

        static map<string, Variable*> hashtab;
};

#endif // VARIABLE_H
