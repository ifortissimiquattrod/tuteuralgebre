#include "variable.h"

std::map<string, Variable*> Variable::hashtab;


/* on construit l'objet variable et on ins�re dans le tableau des symboles */
Variable::Variable(string name, double value)
:_name(name), _value(value)
{
    Variable::hashtab[_name] = this;
}

Variable& Variable::operator=(const Variable& var)
{
}

Variable::~Variable()
{
	hashtab.erase(_name);
}

/** @brief (one liner)
  *
  * (documentation goes here)
  */
/* on affiche la variable */
double Variable::eval() const
{
    return _value;
}

Variable* Variable::getVariable(string name)
{
	return hashtab[name];
}

Variable* Variable::getAssg(string name, double value)
{
    Variable *newvar = hashtab[name];
    if(newvar !=NULL)
    {
        newvar->_value=value;
    }
    else newvar=new Variable(name, value);
}
