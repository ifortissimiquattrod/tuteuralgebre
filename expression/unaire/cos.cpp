#include "cos.h"
#include <cmath>

Cos::Cos(Expression* exp)
:Unaire::Unaire(exp)
{
}

Cos::~Cos()
{
    //dtor
}

double Cos::eval() const
{
    return cos(Unaire::eval());
}
