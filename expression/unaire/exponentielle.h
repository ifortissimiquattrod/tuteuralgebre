#ifndef EXPONENTIELLE_H
#define EXPONENTIELLE_H

#include "unaire.h"


class Exponentielle : public Unaire
{
    public:
        Exponentielle(Expression* exp);
        virtual ~Exponentielle();
        virtual double eval() const;
};

#endif // EXPONENTIELLE_H
