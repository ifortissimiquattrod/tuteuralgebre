#ifndef SIN_H
#define SIN_H

#include "unaire.h"


class Sin : public Unaire
{
    public:
        Sin(Expression* exp);
        virtual ~Sin();
        virtual double eval() const;
};

#endif // SIN_H
