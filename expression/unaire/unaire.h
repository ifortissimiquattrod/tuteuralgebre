#ifndef OPERATEURUNAIRE_H
#define OPERATEURUNAIRE_H

#include "../expression.h"


class Unaire : public Expression
{
    public:
        Unaire(Expression*);
        virtual ~Unaire();
        virtual double eval() const;
    private:
        const Expression* operand;
};

#endif // OPERATEURUNAIRE_H
