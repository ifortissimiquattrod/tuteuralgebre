#ifndef COS_H
#define COS_H

#include "unaire.h"

class Cos : public Unaire
{
    public:
        Cos(Expression* exp);
        virtual ~Cos();
        virtual double eval() const;
};

#endif // COS_H
