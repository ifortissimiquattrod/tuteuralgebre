#ifndef DIFFERENCE_H
#define DIFFERENCE_H

#include "binaire.h"

class Difference : public Binaire
{
    public:
        Difference(Expression *_value1, Expression *_value2);
        virtual ~Difference();
        virtual double eval() const;
};

#endif // DIFFERENCE_H
