#ifndef PRODUIT_H
#define PRODUIT_H

#include "binaire.h"

class Produit : public Binaire
{
    public:
        Produit(Expression *_value1, Expression *_value2);
        virtual ~Produit();
        virtual double eval() const;
};

#endif // PRODUIT_H