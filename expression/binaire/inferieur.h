#ifndef INFERIEUR_H
#define INFERIEUR_H

#include "condition.h"

class Inferieur : public Condition
{
    public:
        Inferieur(Expression *_value1, Expression *_value2);
        virtual ~Inferieur();
        virtual double eval() const;
};

#endif // INFERIEUR_H
