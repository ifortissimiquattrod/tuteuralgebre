#ifndef BINAIRE_H
#define BINAIRE_H

#include "../expression.h"

class Binaire : public Expression
{
    public:
        Binaire(Expression * _value1, Expression * _value2);
        virtual ~Binaire();
        virtual double eval() const=0;
    protected:
        Expression *value1;
        Expression *value2;
};

#endif // BINAIRE_H
