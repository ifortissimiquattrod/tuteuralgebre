#ifndef SUPERIEUR_H
#define SUPERIEUR_H

#include "condition.h"

class Superieur : public Condition
{
    public:
        Superieur(Expression *_value1, Expression *_value2);
        virtual ~Superieur();
        virtual double eval() const;
};


#endif // SUPERIEUR_H
