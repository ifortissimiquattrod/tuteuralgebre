#ifndef DIVISION_H
#define DIVISION_H

#include "binaire.h"

class Division : public Binaire
{
    public:
        Division(Expression *_value1, Expression *_value2);
        virtual ~Division();
        virtual double eval() const;
};

#endif // DIVISION_H