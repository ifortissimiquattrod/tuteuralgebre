#ifndef SOMME_H
#define SOMME_H

#include "binaire.h"

class Somme : public Binaire
{
    public:
        Somme(Expression *_value1, Expression *_value2);
        virtual ~Somme();
        virtual double eval() const;
};

#endif // SOMME_H