#ifndef CONDITION_H
#define CONDITION_H

#include "../expression.h"

class Condition : public Binaire
{
    public:
        Condition(Expression * _value1, Expression * _value2);
        virtual ~Condition();
        virtual double eval() const=0;
};

#endif // CONDITION_H
