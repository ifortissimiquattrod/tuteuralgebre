#include "bloc.h"

Bloc::Bloc():
	name(_name)
{
}

Bloc::Bloc(String _name, Expression *_exp):
    name(_name)
{
	expressions.push_back(_exp);
}

Bloc(String _name, list<Expression*> _exp):
	name(_name), expressions(_exp)
{
}

Bloc::~Bloc()
{
}

double Bloc::eval() const
{
	list<Expression*>::const_iterator it;
    for (it = expressions.begin(); it != expressions.back(); it++)
    {
        (*it).eval();
    }
	return expressions.back()->eval();
}

Bloc::Bloc add(Expression *_exp)
{
	expressions.push_back(_exp);
}