#define BLOC_H
#include "../expression.h"
#include <list>

using namespace std;

class Bloc: public Expression
{
    public:
		Bloc(String _name);
        Bloc(String _name, Expression *_exp);
		Bloc(String _name, list<Expression*> _exp);
        virtual ~Bloc();
        virtual double eval() const;
		Bloc add(Expression *_exp);

    private:
		String name;
        list<Expression*> expressions;
};

#endif // 	BLOC_H