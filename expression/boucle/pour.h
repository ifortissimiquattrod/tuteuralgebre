#define POUR_H
#include "../expression.h"
#include "bloc.h"

using namespace std;

class Pour: public Expression
{
    public:
        Pour(Expression *_init, Condition *_cond, Expression *_assg, Bloc *_bloc);
        virtual ~Pour();
        virtual double eval() const;

    private:
		Expression *init;
		Expression *assg;
		Condition *cond;
		Bloc *bloc;
};

#endif // POUR_H