#define SI_ALORS_SINON_H
#include "../expression.h"

using namespace std;

class Si_alors_sinon: public Expression
{
    public:
        Si_alors_sinon(Condition *_cond, Expression *_value1, Expression *_value2);
        virtual ~Si_alors_sinon();
        virtual double eval() const;

    private:
		Condition *cond;
        Expression *value1;
        Expression *value2;
};

#endif // SI_ALORS_SINON_H