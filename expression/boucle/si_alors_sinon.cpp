#include "si_alors_sinon.h"
#include <limits.h>

Si_alors_sinon::Si_alors_sinon(Condition *_cond, Expression *_value1, Expression *_value2):
    cond(_cond), value1(_value1), value2(_value2)
{

}

Si_alors_sinon::~Si_alors_sinon()
{
}

double Si_alors_sinon::eval() const
{
	if (cond->eval()==1.0) value1->eval();
	else value2->eval();
	return LONG_MAX;
}