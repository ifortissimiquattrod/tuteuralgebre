#ifndef CONDITIONNEL_H
#define CONDITIONNEL_H
#include "../expression.h"

using namespace std;

class Conditionnel : public Expression
{
    public:
        Conditionnel(Condition *_cond, Expression *_value1, Expression *_value2);
        virtual ~Coditionnel();
        virtual double eval() const;

    private:
		Condition *cond;
        Expression *value1;
        Expression *value2;
};

#endif // CONDITIONNEL_H
