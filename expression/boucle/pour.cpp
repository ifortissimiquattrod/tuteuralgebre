#include "pour.h"
#include <limits.h>

Pour::Pour(Expression *_init, Condition *_cond, Expression *_assg, Bloc *_bloc):
    init(_init), cond(_cond), assg(_assg), bloc(_bloc)
{

}

Pour::~Pour()
{
}

double Pour::eval() const
{
	for(init->eval(); cond->eval()==1.0; assg->eval())
	{	
		bloc->eval();
	}
	return LONG_MAX;
}