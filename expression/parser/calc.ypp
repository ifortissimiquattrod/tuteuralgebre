%{
	extern "C" {
		int yylex();
	}
	#include <math.h>
	#include <stdio.h>
	#include "../headerexp.h"
	#include "calc.tab.hpp"
	void yyerror (char const *);
	double tmp;
%}

%union{
	Expression *espr;
	Condition *cond;
	double double_t;
	char *char_t;
}

%error-verbose
%token <double_t>NUM_REAL
%token <char_t>VAR
%token SIN COS EXP IF THEN ELSE
%left '+'
%left '-'
%left '*'
%left '/'
%left '?'
%left ':'

%type <espr>right
%type <char_t>left
%type <espr>assg
%type <espr>right_exp
%type <cond>cond_exp

%start instr_list

%%

instr_list: instr_list instr
		  | instr
		  ;

instr: assg ';'						    {printf("%.2f\n",$1->eval());}
	   | right_exp	';'					{printf("%.2f\n",$1->eval());}
	 ;

assg: left '=' right_exp				{$$=new Variable($1,$3->eval());}
	;

left: VAR			{$$=$1;}
    | IF '(' cond_exp ')' THEN right ELSE right     {$$=new Si_alors_sinon($3,$6,$8);}
	;

right_exp: right '+' right				            {$$=new Somme($1,$3);}
	| right '-' right				                {$$=new Difference($1,$3);}
	| right '*' right				                {$$=new Produit($1,$3);}
	| right '/' right				                {$$=new Division($1,$3);}
	| '(' right ')'					                {$$=$2;}
	| SIN '(' right ')'				                {$$=new Sin($3);}
	| COS '(' right ')'				                {$$=new Cos($3);}
	| EXP '(' right ')'				                {$$=new Exponentielle($3);}
	| '(' cond_exp ')' '?' right ':' right          {$$=new Conditionnel($2,$5,$7);}
	;

cond_exp: right '<=' right              {$$=new Inferieur($1,$3);}
        | right '>=' right              {$$=new Superieur($1,$3);}
        ;

right: NUM_REAL						    {$$=new Constante($1);}
	| VAR								{$$=Variable::getVariable($1);}
    ;

%%

void yyerror (char const *s)
{
	fprintf(stderr, "%s\n", s);
}

int main()
{
	yyparse();
	return 0;
}
