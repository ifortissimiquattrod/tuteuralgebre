#ifndef EVALUATEPROGRAM_H
#define EVALUATEPROGRAM_H

#include "menu_option.h"
#include <string>

extern int yyparse_from_file (const char*);

class EvaluateProgram : public MenuOption
{
    public:
        EvaluateProgram(string name);
        virtual ~EvaluateProgram();

        virtual void execute();

    private:

};

#endif // EVALUATEPROGRAM_H
