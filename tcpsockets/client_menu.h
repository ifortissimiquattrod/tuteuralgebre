/**
 * It's a menu containing the possible options for the client.
 * It can print the menu and then call the correct options.
 * It is linked to the options by using the Command Pattern
 */

#ifndef CLIENTMENU_H
#define CLIENTMENU_H

#include "menu_option.h"
#include <vector>
#include <memory>

using namespace std;

class ClientMenu
{
    public:
        ClientMenu();
        virtual ~ClientMenu();
        void addOption(shared_ptr<MenuOption>);
        void executeOption(int);
        int getOptionsNumber();

        friend ostream& operator<<(ostream& os, const ClientMenu& menu);

    private:
        vector<shared_ptr<MenuOption>> options;
};

#endif // CLIENTMENU_H
