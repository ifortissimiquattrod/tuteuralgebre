#ifndef MENUOPTION_H
#define MENUOPTION_H

#include <string>


using namespace std;

class MenuOption
{
    public:
        MenuOption(string name) :_name(name) {};
        virtual ~MenuOption() {}

        virtual void execute() = 0;
        virtual string getName() { return _name; }

        friend ostream& operator<<(ostream& os, MenuOption& option)
        {
            os << option._name;
            return os;
        }

    private:
        string _name;
};

#endif // MENUOPTION_H
