#ifndef SENDPROGRAM_H
#define SENDPROGRAM_H

#include "menu_option.h"
#include "tcpconnector.h"
#include <string>

class SendProgram : public MenuOption
{
    public:
        SendProgram(string name, string ip, int port);
        virtual ~SendProgram();

        virtual void execute();
    private:
        std::ifstream requestFile();
        TCPStream* connect();
        int _port;
        string _ip;
};

#endif // SENDPROGRAM_H
