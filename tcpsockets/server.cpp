#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fstream>
#include <string>
#include "tcpacceptor.h"

int main(int argc, char** argv)
{
    if (argc < 2 || argc > 4)
    {
        printf("usage: server <port> [<ip>]\n");
        exit(1);
    }

    TCPStream* stream = NULL;
    TCPAcceptor* acceptor = NULL;
    if (argc == 3)
    {
        acceptor = new TCPAcceptor(atoi(argv[1]), argv[2]);
    }
    else
    {
        acceptor = new TCPAcceptor(atoi(argv[1]));
    }
    if (acceptor->start() == 0)
    {
        while (1)
        {
            stream = acceptor->accept();
            if (stream != NULL)
            {
                size_t len;
                char buffer[256];
                len = stream->receive(buffer, sizeof(buffer));
                buffer[len] = '\0';
                std::cout << buffer << std::endl;
                if(len > 0)
                {
                    if(string(buffer) == string("Sending file..."))
                    {
                        //receive title
                        len = stream->receive(buffer, sizeof(buffer));
                        buffer[len] = '\0';
                        std::cout << buffer << std::endl;
                        if(len > 0)
                        {
                            std::ofstream ostream;
                            ostream.open(buffer);
                            while ((len = stream->receive(buffer, sizeof(buffer))) > 0)
                            {
                                buffer[len] = '\0';
                                string line(buffer);
                                ostream << line << std::endl;
                                std::cout << line << std::endl;
                            }
                            ostream.close();
                            stream->send("Received!", string("Received!").size());
                        }
                    }
                }
            }
            /*while ((len = stream->receive(buffer, sizeof(buffer))) > 0) {
                buffer[len] = NULL;
                printf("received - %s\n", buffer);
                stream->send(line, len);
            }*/
            delete stream;
        }
    }
}
