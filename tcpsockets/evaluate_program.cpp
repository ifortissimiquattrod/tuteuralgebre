#include "evaluate_program.h"
#include <iostream>
#include <fstream>

EvaluateProgram::EvaluateProgram(string name) :MenuOption::MenuOption(name)
{
    //ctor
}

EvaluateProgram::~EvaluateProgram()
{
    //dtor
}

void EvaluateProgram::execute()
{
    string filename;
    do{
        std::cout << "insert a file: " ;
        std::cin >> filename;
        std::ifstream istream(filename.c_str());
        if(istream.good()){
            istream.close();
            yyparse_from_file(filename.c_str());
            break;
        }
    } while(1);
}
