#include "tcpconnector.h"
#include "menu_option.h"
#include "send_program.h"
#include "client_menu.h"
#include "evaluate_program.h"
#include <stdlib.h>
#include <iostream>

using namespace std;

/*
 * MOCK yyparse
 */
 int yyparse_from_file (const char*);

int main(int argc, char* argv[]){
    if (argc != 3) {
        cout << "usage: " << argv[0] << " <port> <ip>\n";
        exit(1);
    }

    ClientMenu menu;
    shared_ptr<MenuOption> sp1(new SendProgram("Send a file", argv[2], atoi(argv[1])));
    shared_ptr<MenuOption> sp2(new EvaluateProgram("Evaluate a program"));
    menu.addOption(sp1);
    menu.addOption(sp2);
    string choix = "";
    int iChoix;
    do{
        cout << menu << endl;
        cin >> choix;
        iChoix = atoi(choix.c_str());
        if(iChoix > 0 && iChoix <= menu.getOptionsNumber()){
            menu.executeOption(iChoix-1);
        } else {
            std::cout << "Not valid choice!" << endl;
        }
    } while(iChoix > 0 && iChoix <= menu.getOptionsNumber());
    return 0;

}

int yyparse_from_file (const char* buf){
    cout << "yyparse" << endl;
    return 0;
}
/*
    int len;
    string message;
    char line[256];
    TCPConnector* connector = new TCPConnector();
    TCPStream* stream = connector->connect(argv[2], atoi(argv[1]));
    if (stream) {
        message = "Is there life on Mars?";
        stream->send(message.c_str(), message.size());
        printf("sent - %s\n", message.c_str());
        len = stream->receive(line, sizeof(line));
        line[len] = NULL;
        printf("received - %s\n", line);
        delete stream;
    }

    stream = connector->connect(argv[2], atoi(argv[1]));
    if (stream) {
        message = "Why is there air?";
        stream->send(message.c_str(), message.size());
        printf("sent - %s\n", message.c_str());
        len = stream->receive(line, sizeof(line));
        line[len] = NULL;
        printf("received - %s\n", line);
        delete stream;
    }
    exit(0);*/
