#include "send_program.h"
#include <iostream>
#include <fstream>

SendProgram::SendProgram(string name, string ip, int port)
:_ip(ip), _port(port), MenuOption::MenuOption(name)
{
}

SendProgram::~SendProgram()
{
    //dtor
}

void SendProgram::execute()
{
    std::cout << "file to send: ";
    string filename;
    std::cin >> filename;
    std::ifstream istream(filename.c_str());
    if(!istream.good()){
        throw 1; //Error in file reading
    }
    TCPStream* stream = connect();
    //Sends "Sending file..." to notify that is sending a file
    string message1 = "Sending file...";
    stream->send(message1.c_str(), message1.size());
    std::cout << message1 << std::endl;
    stream->send(filename.c_str(), filename.size());
    std::cout << "Sending filename: " << filename << endl;
    while (getline(istream, message1)){
        stream->send(message1.c_str(), message1.size());
        std::cout << message1 << endl;
    }
    istream.close();
    delete stream;
}

TCPStream* SendProgram::connect()
{
    TCPConnector* connector = new TCPConnector();
    TCPStream* stream = connector->connect(_ip.c_str(), _port);
    if(stream){
        return stream;
    } else {
        throw 2; //Socket error
    }
}
