#ifndef CONSTANTE_H
#define CONSTANTE_H

#include "expression.h"


class Constante : public Expression
{
    public:
        Constante(const double _value);
        virtual ~Constante();
        virtual double eval() const;
    private:
        const double value;
};

#endif // CONSTANTE_H
