#ifndef OPERATEURUNAIRE_H
#define OPERATEURUNAIRE_H

#include "expression.h"


class OperateurUnaire : public Expression
{
    public:
        OperateurUnaire(Expression*);
        virtual ~OperateurUnaire();
        virtual double eval() const;
    private:
        const Expression* operand;
};

class Sin : public OperateurUnaire
{
    public:
        Sin(Expression* exp);
        virtual ~Sin();
        virtual double eval() const;
};

class Cos : public OperateurUnaire
{
    public:
        Cos(Expression* exp);
        virtual ~Cos();
        virtual double eval() const;
};

class Exponentielle : public OperateurUnaire
{
    public:
        Exponentielle(Expression* exp);
        virtual ~Exponentielle();
        virtual double eval() const;
};

#endif // OPERATEURUNAIRE_H
