#ifndef VARIABLE_H
#define VARIABLE_H

#include "expression.h"
#include <string>

class Variable : public Expression
{
    public:
        Variable(string name, double value);
        virtual ~Variable();
        virtual double eval() const;

    private:
        const string _name;
        double _value;
};

#endif // VARIABLE_H
