#ifndef EXPRESSION_H
#define EXPRESSION_H
//#include <set>

using namespace std;

class Expression
{
    public:
        virtual double eval() const = 0;

        static void toutLiberer();

    private:
        //static set<Expression*> _pool;
};

#endif // EXPRESSION_H
