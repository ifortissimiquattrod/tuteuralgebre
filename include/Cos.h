#ifndef COS_H
#define COS_H

#include "Expression.h"


class Cos : public Expression
{
    public:
        Cos(Expression& exp);
        virtual ~Cos();
        virtual double eval() const;
};

#endif // COS_H
