#ifndef BINAIRE_H
#define BINAIRE_H

#include "expression.h"

class Binaire : public Expression
{
    public:
        Binaire(Expression * _value1, Expression * _value2);
        virtual ~Binaire();
        virtual double eval() const=0;
    protected:
        Expression *value1;
        Expression *value2;
};

class Somme : public Binaire
{
    public:
        Somme(Expression *_value1, Expression *_value2);
        virtual ~Somme();
        virtual double eval() const;
};

class Difference : public Binaire
{
    public:
        Difference(Expression *_value1, Expression *_value2);
        virtual ~Difference();
        virtual double eval() const;
};

class Produit : public Binaire
{
    public:
        Produit(Expression *_value1, Expression *_value2);
        virtual ~Produit();
        virtual double eval() const;
};

class Division : public Binaire
{
    public:
        Division(Expression *_value1, Expression *_value2);
        virtual ~Division();
        virtual double eval() const;
};

class Superieur : public Binaire
{
    public:
        Superieur(Expression *_value1, Expression *_value2);
        virtual ~Superieur();
        virtual double eval() const;
};

class Inferieur : public Binaire
{
    public:
        Inferieur(Expression *_value1, Expression *_value2);
        virtual ~Inferieur();
        virtual double eval() const;
};
#endif // BINAIRE_H
