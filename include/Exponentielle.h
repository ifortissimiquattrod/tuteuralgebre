#ifndef EXPONENTIELLE_H
#define EXPONENTIELLE_H

#include "Expression.h"


class Exponentielle : public Expression
{
    public:
        Exponentielle(Expression& exp);
        virtual ~Exponentielle();
        virtual double eval() const;
};

#endif // EXPONENTIELLE_H
