#include "binaire.h"

Inferieur::Inferieur(Expression *_value1, Expression *_value2):
    Binaire::Binaire(_value1, _value2)
{
}

Inferieur::~Inferieur()
{
}

double Inferieur::eval() const
{
    if(value1->eval()<=value2->eval())
        return 0.0;
    else return 1.0;
}


