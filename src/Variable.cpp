#include "expression.h"
#include "variable.h"

Variable::Variable(string name, double value)
:_name(name), _value(value)
{
}

Variable::~Variable()
{
    //dtor
}

/** @brief (one liner)
  *
  * (documentation goes here)
  */
double Variable::eval() const
{
    return _value;
}
