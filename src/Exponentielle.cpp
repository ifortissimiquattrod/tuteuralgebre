#include "expression.h"
#include "unaire.h"
#include <cmath>

Exponentielle::Exponentielle(Expression* exp)
:OperateurUnaire::OperateurUnaire(exp)
{
    //ctor
}

Exponentielle::~Exponentielle()
{
    //dtor
}

double Exponentielle::eval() const
{
    return exp(OperateurUnaire::eval());
}
