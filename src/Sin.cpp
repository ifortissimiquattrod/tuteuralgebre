#include "expression.h"
#include "unaire.h"
#include <cmath>

Sin::Sin(Expression* exp)
: OperateurUnaire::OperateurUnaire(exp)
{
}

Sin::~Sin()
{
    //dtor
}

double Sin::eval() const
{
    return sin(OperateurUnaire::eval());
}
