#include "unaire.h"

OperateurUnaire::OperateurUnaire(Expression* exp)
{
    operand = exp;
}

OperateurUnaire::~OperateurUnaire()
{

}

double OperateurUnaire::eval() const
{
    return operand->eval();
}
