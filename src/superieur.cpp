#include "binaire.h"

Superieur::Superieur(Expression *_value1, Expression *_value2):
    Binaire::Binaire(_value1, _value2)
{
}

Superieur::~Superieur()
{
}

double Superieur::eval() const
{
    if(value1->eval()>=value2->eval())
        return 0.0;
    else return 1.0;
}

