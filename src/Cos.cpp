#include "expression.h"
#include "unaire.h"
#include <cmath>

Cos::Cos(Expression* exp)
:OperateurUnaire::OperateurUnaire(exp)
{
}

Cos::~Cos()
{
    //dtor
}

double Cos::eval() const
{
    return cos(OperateurUnaire::eval());
}
