#include <iostream>
#include "constante.h"
#include "binaire.h"
#include "expression.h"
#include "unaire.h"
#include "variable.h"

using namespace std;


int main()
{
    Sin sin(new Variable("x", 0.0));
    Cos cos(new Variable("y", 0.0));
    Exponentielle exp(new Variable("z", 1.0));
    cout << "sin(0.0) = " << sin.eval() << endl;
    cout << "exp(1.0) = " << exp.eval() << endl;
    cout << "cos(0.0) = " << cos.eval() << endl;
    Somme *s = new Somme(new Constante(1.0), new Constante(2.0));
    cout<< "s=" << s->eval() << endl;
    return 0;
}
